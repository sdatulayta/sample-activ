<div class="form-group">
	{{ Form::label('employee_name', 'Employee Name') }}
	{{ Form::text('employee_name', null, ['class' => 'form-control']) }}
	@if ($errors->has('employee_name'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
			{{ $errors->first('employee_name', ':message') }}
		</div>
	@endif
</div>
<div class="form-group">
	{{ Form::label('email', 'Email') }}
	{{ Form::text('email', null, ['class' => 'form-control']) }}
	@if ($errors->has('email'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
			{{ $errors->first('email', ':message') }}
		</div>
	@endif
</div>
<div class="form-group">
	{{ Form::label('contact_number', 'Contact Number') }}
	{{ Form::text('contact_number', null, ['class' => 'form-control']) }}
	@if ($errors->has('contact_number'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
			{{ $errors->first('contact_number', ':message') }}
		</div>
	@endif
</div>
<div class="form-group">
	{{ Form::label('address', 'Address') }}
	{{ Form::textarea('address', null, ['class' => 'form-control', 'rows' => 3]) }}
	@if ($errors->has('address'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
			{{ $errors->first('address', ':message') }}
		</div>
	@endif
</div>
<div class="form-group">
	{{ Form::label('postal_code', 'Postal Code') }}
	{{ Form::text('postal_code', null, ['class' => 'form-control']) }}
	@if ($errors->has('postal_code'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
			{{ $errors->first('postal_code', ':message') }}
		</div>
	@endif
</div>
<div class="form-group">
	{{ Form::submit('Submit', ['class' => 'btn btn-success pull-right']) }}
</div>