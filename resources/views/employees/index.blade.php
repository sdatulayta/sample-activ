@extends('layout')

@section('content')
	<div class="col-md-12">
		<h2>List of all Employee</h2>
		<hr>
		<div>
			<a href="{{ route('employees.create') }}" class="btn btn-primary pull-right">Create</a>
			<button class="btn btn-danger" data-toggle="modal" data-target="#confirmModal">Delete All</button>
		</div>
		<br>
		@if(Session::has('info'))
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
				{{ Session::get('info') }}
			</div>
		@endif
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Contact Number</th>
					<th>Address</th>
					<th colspan="2">Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($employees as $employee)
				<tr>
					<td>{{ $employee->employee_name }}</td>
					<td>{{ $employee->email }}</td>
					<td>{{ $employee->contact_number }}</td>
					<td>{{ $employee->address }}&nbsp;{{ $employee->postal_code }}</td>
					<td width="20px">
						<a class="btn btn-sm btn-success" href="{{ route('employees.edit', $employee->id) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true">Edit</a>
					</td>
					<td width="20px">
						{!! Form::open(['route' => ['employees.destroy', $employee->id], 'method' => 'delete']) !!}
						<button class="btn btn-sm btn-danger pull"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Delete</button>
						{!! Form::close() !!}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! $employees->render() !!}


		<div class="modal fade" id="confirmModal" role="dialog">
			<div class="modal-dialog" role="document">
				{!! Form::open(['method'=>'post', 'url'=>'employees/truncate']) !!}
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"><span class="glyphicon glyphicon-exclamation-sign"></span></h4>
					</div>
					<div class="modal-body">
						<h3>Are you sure you want to delete all the employee?</h3>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit"  class="btn btn-danger">OK</button>
					</div>
				</div>
				{!! Form::close()!!}
			</div>
		</div>
	</div>
@endsection