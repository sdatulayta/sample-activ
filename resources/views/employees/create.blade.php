@extends('layout')

@section('content')
	<div class="col-md-12">
		<h2>
			Create Employee
			<a class="btn btn-default pull-right" href="{{ route('employees.index') }}"><span class="glyphicon glyphicon-chevron-left"></span>Back</a>
		</h2>
		<hr>
		{!! Form::open(['route' => 'employees.store']) !!}
			@include('employees.form')
		{!! Form::close() !!}
	</div>
@endsection