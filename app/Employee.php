<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'employee_name', 'email', 'contact_number', 'address', 'postal_code'
    ];
}
